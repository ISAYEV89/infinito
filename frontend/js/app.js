import $ from 'jquery';
import 'bootstrap'
import 'owl.carousel';
import 'jquery-validation';


$(document).ready(function(){

    $('ul.tabs li').click(function(){
        var tab_id = $(this).attr('data-tab');

        $('ul.tabs li').removeClass('current');
        $('.tab-content').removeClass('current');

        $(this).addClass('current');
        $("#"+tab_id).addClass('current');
    })

    $('.slider-testimonial').owlCarousel({
        responsive:{
            0:{
                items:1
            }
        }
    });

    $('.slider-partners').owlCarousel({
        margin:50,
        responsive:{
            0:{
                items:1
            },
            500:{
                items:2
            },
            800:{
                items:3
            },
            1000:{
                items:4
            }
        }
    });


    $('.slider-hero-area').owlCarousel({
        margin:10,
        loop:true,
        dots: true,
        nav: true,
        items: 1
    });


/*    $('.slider-hero-area').on('initialized.owl.carousel changed.owl.carousel', function(e) {
        if (!e.namespace)  {
            return;
        }
        var carousel = e.relatedTarget;
        $('.slider-hero-area__item').text(carousel.relative(carousel.current()) + 1 + '/' + carousel.items().length);
    }).owlCarousel({
        items: 1,
        loop:true,
        margin:0,
        nav:true
    });*/

    $(".js-anchor-link").on('click', function (event) {
        event.preventDefault();
        var hash = this.hash;
        $('html, body').animate({
            scrollTop: $(hash).offset().top
        }, 800, function () {
            window.location.hash = hash;
        });
    });


})